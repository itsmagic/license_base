# License Base -- _used to license software_

## Intent
Allow software to be sold and licensed


## Implementation

* Listener and thread

```
from license_base import LicenseBase
from wx.lib.wordwrap import wordwrap 

dispatcher.connect(self.license_message,
                    signal="License",
                    sender=dispatcher.Any)

self.license_thread = LicenseBase(name=self.name, enforce_serial=True)
self.license_thread.daemon = True
self.license_thread.start()
```
* Functions
```
    def update_required(self, url):
        """Show the update page"""
        dlg = wx.MessageDialog(parent=self,
                               message='An update is available. \rWould you like to go to the download page?',
                               caption='Update available',
                               style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            webbrowser.open(url)

    def license_message(self, data):
        """We either request a serial number or show a message"""
        if "get_serial" in data:
            wx.CallAfter(self.license_get_serial, data['message'])
        if "show_message" in data:
            wx.CallAfter(self.license_show_message, data['message'])
        if "manual_activation" in data:
            wx.CallAfter(self.save_manual_activation, data['cipher_text'], data['serial'], data['stage_two'])
        if "complete" in data:
            self.Show()
            if "update_url" in data:
                update_url = data['update_url']
                if update_url:
                    self.update_required(url=update_url)


    def save_manual_activation(self, cipher_text, serial, stage_two):
        self.Hide()
        if stage_two:
            # Stage two
            open_file_dialog = wx.FileDialog(self, message="Load Manual Activation",
                                             defaultDir=self.storage_path,
                                             defaultFile="",
                                             wildcard="Custom Files (*.lic;)|*.lic;",
                                            style=wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
            if open_file_dialog.ShowModal() == wx.ID_OK:
                license_path = open_file_dialog.GetPath()
                success = self.license_thread.manual_activation(license_path, serial)
                if success:
                  self.Show()
                  return
                else:
                    dlg = wx.MessageDialog(parent=self,
                               message=wordwrap("Unable to use that file for manual activation",350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK)
                    dlg.ShowModal()

    
        dlg = wx.MessageDialog(parent=self,
                               message=wordwrap("Unable to contact server.\nWould you like to download the manual activation?",350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK | wx.CANCEL)
        if dlg.ShowModal() == wx.ID_OK:
            dlg = wx.FileDialog(self,
                                message='Save manual activation',
                                defaultDir=self.storage_path,
                                defaultFile=f"{serial}.txt",
                                wildcard="TXT files (*.txt)|*.txt",
                                style=wx.FD_SAVE)
            if dlg.ShowModal() == wx.ID_OK:
                file_path = dlg.GetPath()
                with open(file_path, 'w') as cipher_text_file:
                    cipher_text_file.write(cipher_text)
        wx.CallAfter(self.on_exit, None)
            


    def license_get_serial(self, message):
        self.Hide()
        dlg = wx.TextEntryDialog(parent=self,
                           message=f"{message}\nPlease enter your serial number",
                           caption=f'{self.name} Serial Number Required',
                           style=wx.OK | wx.CANCEL)

        if dlg.ShowModal() == wx.ID_OK:
            serial = dlg.GetValue()
            self.license_thread.verify_serial_with_server(serial)
            self.Show()
        else:
            self.license_show_message("This software requires a valid serial number to run.")

    def license_show_message(self, message: str, exit: bool=True):
        dlg = wx.MessageDialog(parent=self,
                               message=wordwrap(message,350, wx.ClientDC(self)),
                               caption='Unlicensed',
                               style=wx.OK)
        dlg.ShowModal()
        if exit:
            wx.CallAfter(self.on_exit, None)


```

* Requirements

```
git+https://bitbucket.org/itsmagic/license_base.git
```

## Notes
* require_serial_number if set True will not allow program to run with out serial number
