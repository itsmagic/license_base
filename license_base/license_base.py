'''Magicsoftware licensing'''

import base64
import json
import logging
import requests
import subprocess
import winreg

from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import padding
from packaging.version import parse
from threading import Thread

logger = logging.getLogger("LicenseBase")
logger.setLevel(logging.INFO)


class LicenseBase(Thread):
    """License Base Thread"""

    def __init__(self, name: str, version: str, message_callback, server_uri: str = "https://magicsoftware.ornear.com", enforce_serial=False):
        self.shutdown = False
        self.application_name = name
        self.application_version = version
        self.message_callback = message_callback
        self.update_url = None
        self.enforce_serial = enforce_serial
        self.server_uri = server_uri
        self.check_in_uri = server_uri + "/register/checkin/"

        self.public_key = serialization.load_pem_public_key(b"""-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA28ScQvwVmIFwjOMXvo10
3hotQPBFITXos8swfGH+E/lHQpj3q1pvY76cfXZjSGH/o3kvFzKLzc24kVJqW2sS
l8G88WHnMJzmTeOwMxpSX3wNAPQIL+Zu53VEkU4n7Ykfx089Y7FMpcF0bsbPwQBd
GAnU3wnxEemArzD8TaltL28/HHSCswNn61R+mk2sGtzZJCSRY4TstLRp8I2u/BLa
RUehs5qhQys6mhjtVQBtXgjNBJqlCJvAzziLZZS/7kNO+8b2Jgi5WpTlmxFidYEQ
X+AQX5RY/39FqHQww9thu16D+/SkTR7gj5V7TaaZQSQglPRrezjSHTtPrZS+f2o/
WQIDAQAB
-----END PUBLIC KEY-----""")

        Thread.__init__(self, name="LicenseBase")

    def shutdown_signal(self, sender):
        """Shut down the thread"""
        self.shutdown = True

    def _get_pc_uuid(self):
        """Gets the PC's uuid"""
        # Leave shell=True, stops a cmd window blinking up after build in pyinstaller
        return subprocess.check_output("wmic csproduct get uuid", shell=True).decode().split()[1]

    def _create_check_in_message(self, pc_uuid: str, serial_number: str, application_name: str, application_version: str) -> str:
        """
        Creates a check in message and returns it as a json string
        """
        app_name = "_".join(application_name.lower().split())
        message = json.dumps({"pc_uuid": pc_uuid,
                              "serial_number": serial_number,
                              "application_name": app_name,
                              "application_version": application_version}).encode()
        return message

    def _encrypt_message(self, public_key, message):
        """Encrypts the message"""
        ciphertext = public_key.encrypt(
            message,
            padding.OAEP(
                mgf=padding.MGF1(algorithm=hashes.SHA256()),
                algorithm=hashes.SHA256(),
                label=None
            )
        )
        return ciphertext

    def _verify_signature(self, message, signature):
        try:
            sig = base64.urlsafe_b64decode(signature.encode())
            self.public_key.verify(
                sig,
                message,
                padding.PSS(
                    mgf=padding.MGF1(hashes.SHA256()),
                    salt_length=padding.PSS.MAX_LENGTH
                ),
                hashes.SHA256()
            )
            return True
        except Exception as error:
            logging.critical(f"Verification error: {error}")
            return False

    def _send_to_server(self, text: str):
        try:
            response = requests.post(self.check_in_uri, data=text)
            if response.status_code == 200:
                my_json = response.json()
                logging.debug(f"Got json: {my_json}")
                return my_json
            else:
                logging.critical(f"Unable to connect to server: {response}")
        except Exception as error:
            logging.error(f"Unable to connect to server: {error}")
        return {}

    def _verify_serial(self, serial) -> str:
        """
        Checks provided serial number is a valid serial number, this doesn't mean it is the correct serial number
        Returns the serial_number or "" if it is invalid
        """
        try:
            serial = serial.strip()
            score = 0
            check_digit = serial[0]
            check_digit_count = 0
            chunks = serial.split('-')
            for chunk in chunks:
                if len(chunk) != 4:
                    logger.critical("Invalid serial number")
                    return ""
                for char in chunk:
                    if char == check_digit:
                        check_digit_count += 1
                    score += ord(char)
            if score >= 1773 and score <= 1800 and check_digit_count == 3:
                return serial
        except Exception as error:
            logger.critical("Invalid serial number")
        # Not valid
        return ""

    def _get_reg_value(self, key):
        try:
            location = winreg.HKEY_CURRENT_USER
            program = winreg.OpenKeyEx(location, fr"SOFTWARE\\Magic Software\\{self.application_name}\\")
            value = winreg.QueryValue(program, key)
            if program:
                winreg.CloseKey(program)
            return value
        except FileNotFoundError:
            return None

    def _set_reg_value(self, key, value):
        try:
            location = winreg.HKEY_CURRENT_USER
            program = winreg.OpenKeyEx(location, fr"SOFTWARE\\Magic Software\\{self.application_name}\\")
            winreg.SetValue(program, key, winreg.REG_SZ, value)
            if program:
                winreg.CloseKey(program)
        except FileNotFoundError:
            logging.critical("Unable to set key")
            return

    def _init_reg(self):
        # Setup the registry, this can be called at any time
        location = winreg.HKEY_CURRENT_USER
        software = winreg.OpenKeyEx(location, r"SOFTWARE\\")

        # Create our software
        top_level = winreg.CreateKey(software, "Magic Software")

        # Create this program
        program = winreg.CreateKey(top_level, self.application_name)

        # Close reg
        if program:
            winreg.CloseKey(program)

    def _check_if_serial_number_required(self, response) -> bool:
        if 'require_serial_number' in response:
            if response['require_serial_number']:
                self._set_reg_value('serial_required', '1')
                return True
            else:
                self._set_reg_value('serial_required', '0')
        return False

    def verify_serial_with_server(self, serial: str):
        """
        Can be run at anytime to verify serial with server?
        """
        # Check if serial number is valid
        serial = self._verify_serial(serial)

        # Get PC UUID
        pc_uuid = self._get_pc_uuid()

        # We need to check in to see if the server is putting us in enforce mode
        # Added self to message so we can access it in manual activation
        self.message = self._create_check_in_message(pc_uuid=pc_uuid,
                                                     serial_number=serial,
                                                     application_name=self.application_name,
                                                     application_version=self.application_version)
        text = {'cipher': base64.urlsafe_b64encode(self._encrypt_message(self.public_key, self.message)).decode()}
        response = self._send_to_server(text)
        if response:
            self.enforce_serial = self._check_if_serial_number_required(response)
            if 'current_version' in response:

                if parse(response['current_version'][1:]) > parse(self.application_version[1:]):
                    self.update_url = f'{self.server_uri}/updates/{"_".join(self.application_name.lower().split())}'

        # Check if we are done, no serial required
        if not self.enforce_serial:
            # We do not require a serial and have already done the check in we are done...
            self.message_callback({"complete": True, "update_url": self.update_url})
            return

        # If we don't have a serial we should ask for one
        if not serial:
            self._request_a_serial_number_from_user()
        else:
            # We have a valid serial number
            # We either are online, offline
            if response != {}:
                self._do_something_based_on_server_response(serial, self.message, response)
            else:
                self._do_something_when_we_cannot_reach_server(serial, self.message, text['cipher'])

    def _request_a_serial_number_from_user(self):
        self.message_callback({"get_serial": True, "message": ""})

    def _do_something_based_on_server_response(self, serial, message, response):
        # Verify
        if 'success' in response:
            if response['success']:
                if self._verify_signature(message=message, signature=response['signature']):
                    logging.info("Signature verified")
                    self._set_reg_value('sig', response['signature'])
                    self._set_reg_value('serial', serial)
                    self.message_callback({"complete": True, "update_url": self.update_url})
                    return
            else:
                # We have an error message from the server
                logging.info(f"Unable to register {response['error']}")
                if self.enforce_serial:
                    self.message_callback({"get_serial": True, "message": f"Unable to register\n{response['error']}\n"})
                return

    def _do_something_when_we_cannot_reach_server(self, serial, message, cipher_text):
        # We were unable to contact or get a proper response from the server
        if self.enforce_serial:
            # Since we cannot reach the server, we could try to use the stored details
            sig = self._get_reg_value('sig')
            if self._verify_signature(message, signature=sig):
                logging.info("Signature verified")
                self.message_callback({"complete": True, "update_url": self.update_url})
                return
            # Ok, maybe a manual activation is what is required
            # First see if we have a serial number stored that matches
            # This means we have already gotten a manual activation
            if self._get_reg_value('manual_serial') == serial:
                self.message_callback({"manual_activation": True, "cipher_text": cipher_text, "serial": serial, "stage_two": True})
            else:
                self._set_reg_value('manual_serial', serial)
                self.message_callback({"manual_activation": True, "cipher_text": cipher_text, "serial": serial, "stage_two": False})

    def manual_activation(self, license_file_path: str, serial: str):
        """
        Try to open the file and manually activate
        """
        try:
            with open(license_file_path, 'r') as f:
                sig = f.read()
        except Exception as error:
            logging.critical("Unable to read license file")
            return False

        if self._verify_signature(message=self.message, signature=sig):
            logging.info("Signature verified")
            self._set_reg_value('manual_serial', "")
            self._set_reg_value('sig', sig)
            self._set_reg_value('serial', serial)
            self.message_callback({"complete": True})
            return True
        return False

    def run(self):
        """
        Run Licensing
        """
        # Initialize the registry
        self._init_reg()
        # First check if we have a stored serial_number / signature
        serial = self._get_reg_value('serial')

        # Only check the registry if we enforce is initialized disabled
        if not self.enforce_serial:
            self.enforce_serial = self._get_reg_value('serial_required') == "1"

        self.verify_serial_with_server(serial)


def messages(data):
    logger.critical(f"Got a License message data: {data}")


if __name__ == "__main__":
    quit()
