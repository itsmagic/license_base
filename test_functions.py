from license_base import LicenseBase

import json

def test_create_message():
    serial = "e8v8-3joa-ve6d-cpv7-e14x"
    message = LicenseBase._create_check_in_message(None, 
                                          pc_uuid="hello there",
                                          serial_number=serial,
                                          application_name="Test Application",
                                          application_version="v1.2.3")
    message_dict = json.loads(message)
    assert message_dict['pc_uuid'] == "hello there"
    assert message_dict['serial_number'] == serial
    assert message_dict['application_name'] == "test_application"
    assert message_dict['application_version'] == "v1.2.3"


def test_validate_serial_number():
    valid_serials = {"e8v8-3joa-ve6d-cpv7-e14x": "e8v8-3joa-ve6d-cpv7-e14x", "               e8v8-3joa-ve6d-cpv7-e14x": "e8v8-3joa-ve6d-cpv7-e14x"}
    invalid_serials = ["e8v8-3joa-", "hellow there", b"fjasldfas", "", b""]

    for valid_serial in valid_serials:
        assert LicenseBase._verify_serial(None, valid_serial) == valid_serials[valid_serial]
    for bad_serial in invalid_serials:
        assert LicenseBase._verify_serial(None, bad_serial) == ""




"""
Manual tests
1. Application and server do not require a license
2. Application requires a license but the server doesn't -- This is an edge case
3. Application does not require a license but server does
4. Both application and server require a license


Repeat above tests with server offline
1. Application and server do not require a license
2. Application requires a license but the server doesn't
3. Application does not require a license but server does
4. Both application and server require a license

Edge case
1. serial_required in reg even though nothing else requires it...

"""