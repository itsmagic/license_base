import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="license-base",
    version="0.1.3",
    author="Jim Maciejewski",
    author_email="magicsoftware@ornear.com",
    description="License Base",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/itsmagic/license_base",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=['requests', 'packaging', 'cryptography'],
    python_requires='>=3.6',
)
